/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.apachepoiwriteexample.student.exam;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vaganovdv
 */
public class StudentLoader
{
    
    public List<Student> loadFromList(){
        List<Student> studentList = new ArrayList<>();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();
        
        student1.setFirstName("Владимир");
        student1.setMiddleName("Владимирович");
        student1.setLastName("Иванов");
        
        student2.setFirstName("Василий");
        student2.setMiddleName("Владимирович");
        student2.setLastName("Петров");
        
        student3.setFirstName("Юури");
        student3.setMiddleName("-");
        student3.setLastName("Такахаси");
        
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        
        for (int i = 0; i < studentList.size(); i++) {
            System.out.println(studentList.get(i).getLastName());
        }
        return studentList;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.apachepoiwriteexample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *  Код для формирования файла Excel
 * 
 * @author vaganovdv
 */
public class ExcelWriter
{

    // Имя файла Excel
    private final String excelFilename ="studentsExcel.xlsx";
    
    public ExcelWriter()
    {
        
    }
    
     public void writeExcelFile()
    {

        System.out.println("Запись файла Excel");

        // Получение сведений о текущем каталоге c использованием стандатного класса Java: 
        // Вызов функции get с пустой строкой возвращает текущий каталог
        Path currentRelativePath = Paths.get("");        
        String directory = currentRelativePath.toAbsolutePath().toString();
            
        System.out.println("Файл Excel будет записан в каталог: " + directory);

        // Формирование полного имени файла:
        //                     текущий каталог      разделитель каталога и имени   имя файла Excel    
        //                           |                  |                             |
        File excelFile = new File(directory + File.separator + excelFilename);

        System.out.println("Полное имя файла: " + excelFile.getAbsolutePath());
        System.out.println("Имя файла: " + excelFile.getName());
        System.out.println("Каталог размещения файла: " + excelFile.getParent());
        

        // Создание книги Excel
        XSSFWorkbook workbook = new XSSFWorkbook();

        // Создание листа в книге
        XSSFSheet studentList = workbook.createSheet("Список студентов - 2023");
      

        // Заполение листа Excel "Список студентов"
        Row header = studentList.createRow(0);         // Создание заголовка -              строка  номер 0
        Row student1 = studentList.createRow(1);       // Строка со сведениямио студенте 1  строка  номер 1
        Row student2 = studentList.createRow(2);       // Строка со сведениямио студенте 2  строка  номер 2
        Row student3 = studentList.createRow(3);       // Строка со сведениямио студенте 3  строка  номер 3
        Row student4 = studentList.createRow(4);       // Строка со сведениямио студенте 3  строка  номер 3
        
        
        // Заполнение заголовка таблицы
        Cell f = header.createCell(0);        f.setCellValue("Фамилия");
        Cell i = header.createCell(1);        i.setCellValue("Имя");
        Cell o = header.createCell(2);        o.setCellValue("Отчество");
        Cell c = header.createCell(3);        c.setCellValue("Курс");
        Cell g = header.createCell(4);        g.setCellValue("Группа");

        // Заполнение строки студента 1
        Cell f1 = student1.createCell(0);        f1.setCellValue("Ваганов");
        Cell i1 = student1.createCell(1);        i1.setCellValue("Дмитрий");
        Cell o1 = student1.createCell(2);        o1.setCellValue("Валерьевич");
        Cell c1 = student1.createCell(3);        c1.setCellValue("1");
        Cell g1 = student1.createCell(4);        g1.setCellValue("СТ-21");

        // Заполнение строки студента 2
        Cell f2 = student2.createCell(0);        f2.setCellValue("Сидоров");
        Cell i2 = student2.createCell(1);        i2.setCellValue("Евгений");
        Cell o2 = student2.createCell(2);        o2.setCellValue("Викторович");
        Cell c2 = student2.createCell(3);        c2.setCellValue("5");
        Cell g2 = student2.createCell(4);        g2.setCellValue("СТ-57");

        // Заполнение строки студента 3
        Cell f3 = student3.createCell(0);        f3.setCellValue("Петровский");
        Cell i3 = student3.createCell(1);        i3.setCellValue("Олег");
        Cell o3 = student3.createCell(2);        o3.setCellValue("Васильевич");
        Cell c3 = student3.createCell(3);        c3.setCellValue("3");
        Cell g3 = student3.createCell(4);        g3.setCellValue("СТ-32");

         // Заполнение строки студента 3
        Cell f4 = student4.createCell(0);        f4.setCellValue("Курбанов");
        Cell i4 = student4.createCell(1);        i4.setCellValue("Илья");
        Cell o4 = student4.createCell(2);        o4.setCellValue("Евгеньевич");
        Cell c4 = student4.createCell(3);        c4.setCellValue("3");
        Cell g4 = student4.createCell(4);        g4.setCellValue("СТ-32");
        
        
        // Запись файла Excel
        OutputStream fileOut;  // Поток для вывода 
        try
        {
            // файловый поток 
            //  |
            fileOut = new FileOutputStream(excelFile.getAbsoluteFile().toString()); // Формирование потока
            workbook.write(fileOut);    // Запись книги Excel в файл
            fileOut.flush();            // Сброс потока на диск
            fileOut.close();            // Закрытие потока    

        } catch (FileNotFoundException ex)
        {
            System.out.println("Ошибка записи файла Excel - не нейден файл: {" + excelFile.getAbsoluteFile() + "");
        } catch (IOException ex)
        {
            System.out.println("Ошибка ввода-вывода при записи  файла: {" + excelFile.getAbsoluteFile() + "");
        }

    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.apachepoiwriteexample;

/**
 *
 * @author vaganovdv
 */
public class ApachePoiWriteExcel
{
    
    public static void main(String[] args)
    {
        // Вывод сообщения 
        System.out.println("Пример программы записи файла Excel");
        
        //Создание экземпляра класса для записи файла Excel
        ExcelWriter writer = new ExcelWriter();
        writer.writeExcelFile();
        
        // Создание экземпляра класса для чтения файла Excel 
        //ExcelReader excelReader = new ExcelReader();
        //excelReader.readExcelFile();
        
        //StudentLoader studentLoader = new StudentLoader();
        //studentLoader.loadFromList();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.apachepoiwriteexample;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author vaganovdv
 */
public class ExcelReader
{

    // Имя файла Excel
    private final String excelFilename = "StudentsExcel.xlsx";

    public void readExcelFile()
    {

        System.out.println("Запись файла Excel");

        // Получение сведений о текущем каталоге
        Path currentRelativePath = Paths.get("");
        String directory = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Файл Excel будет записан в каталог: {" + directory + "}");

        //Формирование полного имени файла:
        //                     текущий каталог      разделитель каталога и имени   имя файла Excel
        //                           |                  |                             |
        File excelFile = new File(directory + File.separator + excelFilename);
        System.out.println("Имя файла: {" + excelFile + "}");

        
        // Этапы чтения файла Excel
        try
        {

            // 1. Открытие существующей книги Excel из файла Excel
            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
            

            // 2. Открытие листа в книге - использется метод getSheet
            XSSFSheet studentList = workbook.getSheet("Список студентов");
            
           

            // 3. Демонстрация 2-х способов чтения файла Excel
            if (studentList != null)
            {
                 readSheetUsingLambda(studentList);      // Использование ламбда выражений (новый способ)
                //readSheetUsingIterator(studentList);    // Использование итераторов (классический способ)
            } else
            {
                System.out.println("Ошибка открытия файла {" + excelFile.getAbsoluteFile() + "}");
            }

        } catch (IOException ex)
        {
            System.out.println("Ошибка открытия файла {" + excelFile.getAbsoluteFile() + "}");
        } catch (InvalidFormatException ex)
        {
            System.out.println("Неверный формат файла Excel {" + excelFile.getAbsoluteFile() + "}");
        }

    }

    /**
     * Чтение листа Excel с использованием лямбда выражения
     *
     * @param studentList
     */
    private void readSheetUsingLambda(Sheet studentList)
    {
        System.out.println("");
        System.out.println("Чтение листа Excel с использованием лямбда выражения");

        // Движение (цикл) по строкам листа 
        studentList.forEach(row ->
        {
            // Движение (цикл) по ячейка
            row.forEach(cell ->
            {
                CellType cellType = cell.getCellTypeEnum();

                switch (cellType)
                {
                    case NUMERIC:
                        System.out.println("Число: " + cell.getNumericCellValue());
                        break;
                    case STRING:
                        System.out.println("Строка: " + cell.getStringCellValue());
                        break;
                    case ERROR:
                        System.out.println("Ошибка чтения ячейки");
                        break;
                }
            });

        });

        
    }

    /**
     * Чтение листа Excel с использованием лямбда выражения
     *
     * @param studentList
     */
    private void readSheetUsingIterator(XSSFSheet studentList)
    {
        System.out.println("");
        System.out.println("Чтение листа Excel с использованием итераторов");
        
        // Создание итератора строк (Row)
        Iterator<Row> iterator = studentList.iterator();
        
        //       Возвращает true, если имеются строки для обработки
        //         |
        while (iterator.hasNext()) // while проверяется истинность выражения в скобках
        { // Цикл движения по строкам (начало)
            
            // Чтение строки (строка содержит список (массив ячеек)
            Row nextRow = iterator.next(); // Обрабатываемая строка навывается nextRow
            
            // Создание итератора, который обрабатывает ячейки (Cell) внутри строки (nextRow)
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            
            //     Просматривает ячейки внутри строки - возращает true,если есть ячейка для обработки
            //        |
            while (cellIterator.hasNext())
            { // Цикл движения по ячейкам (cell)
                
                // Чтение текущей ячейки внутри строки
                Cell cell = cellIterator.next();
                CellType cellType = cell.getCellTypeEnum();
                
                switch (cellType)
                {
                    case NUMERIC:                      // Получение числа из ячейки
                        System.out.println("Число: " + cell.getNumericCellValue());
                        break;
                    case STRING:                     // Получение строки из ячейки
                        System.out.println("Строка: " + cell.getStringCellValue());
                        break;
                    case ERROR:
                        System.out.println("Ошибка чтения ячейки");
                        break;
                }
                
            }
        } // Цикл движения по строкам (окончание)

    }

}
